# PIGS - pokemon in geometric shapes

**TODO:**

**DONE:**
- initialize opencv libs
- getting the shape using findContours
- simplifying contour using approxPoly
- set maxCurr by binary searching the proportion of leng to take
    - do until shape2.size()==3
- color using counting, with masking 

**PASSED:**
- load image using link, instead of using file uploads (CORS not doable)

**EXTENSION:**
- explaination for algo findContours
- explaination for approxPoly
